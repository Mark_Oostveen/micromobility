import 'package:google_maps_flutter/google_maps_flutter.dart';

Marker createMarker(LatLng position, String id){
  return Marker(position: position, markerId: MarkerId(id), flat: false, consumeTapEvents: true, icon: BitmapDescriptor.defaultMarkerWithHue(52));
}