import 'package:http/http.dart' as http;
import 'dart:convert';

String apiurl = "localhost:8080";
String api_version="";

Future<http.Response> post(String relativeurl, {Map<String, String> headers, body, Encoding encoding}){
  http.post(apiurl+ relativeurl, headers: headers, body: body, encoding: encoding);
}