import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:maps_micro_mobility_app/microservices/available_devices.dart';
import 'map.dart';

class HomePage extends StatefulWidget{

  static final CameraPosition _initialposition = CameraPosition(target: LatLng(0, 0) );

  @override
  State<StatefulWidget> createState() {
    return HomePageLoadingState();
  }

}

class HomePageLoadingState extends State<HomePage>{

  GoogleMap _map;

  HomePageLoadingState(){
    if(_map == null){
      _map = GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: HomePage._initialposition,
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
        compassEnabled: true,
        markers: null,
      );

      
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(child: _map),
          AspectRatio(
            aspectRatio: 16/3,
            child: Container(
              color: Colors.grey,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('Profile', style: Theme.of(context).textTheme.title,),
                  Text('filter', style: Theme.of(context).textTheme.title,),
                  Text('History', style: Theme.of(context).textTheme.title,),
                  Text('Settings', style: Theme.of(context).textTheme.title,),
                ],
              ),
            ),
          )
        ]
      )
    );
  }

}