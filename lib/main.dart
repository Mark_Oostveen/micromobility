import 'package:flutter/material.dart';
import 'package:maps_micro_mobility_app/home.dart';
import 'map.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    locationSetup();
    return MaterialApp(
      title: 'Micro services demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage()
    );
  }
}