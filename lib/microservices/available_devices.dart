import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_micro_mobility_app/utils/markerfactory.dart';
import 'package:location/location.dart';
import 'package:maps_micro_mobility_app/utils/api.dart';
import 'package:http/http.dart' as http;

Future<List<Marker>> getAvailableMicroDevices(Location location, double radius) async {
  LocationData currentlocation = await location.getLocation();
  http.Response response = await post("/devices/active_devices/" + currentlocation.latitude.toString() + "/" + currentlocation.longitude.toString() + "/" + radius.toString());
  return [createMarker(LatLng(51.319717, 5.357500), '0'), createMarker(LatLng(51.421310, 5.403610), '1'), createMarker(LatLng(51.441643, 5.469722), '2')];
}